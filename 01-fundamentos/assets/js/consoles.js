console.log ('Hello there!');

// Trabajando con variables.

let number1 = 25;
let number2 = 44;
let number3 = 2;
let number4 = 788;

let suma = 0;
let resta = 0;
let division = 0;
let multiplicacion = 0;

// Con las variables no hay límites.

suma = number1 + number2;
resta = suma - number1;
division = suma / resta;
multiplicacion = division * resta * suma;

console.log(suma);
console.log(resta);
console.log(division);
console.log(multiplicacion);

// En JS hay muchas maneras de trabajar con variables.